var totalProducts=0;
var modifying=false;
var itemToModify={
	img: '', 
	desc: '',
	index: null
}

function addItem(){
	var file=document.getElementById("art").files[0];
	var desc=document.getElementById("description").value;
	var newProduct=document.getElementById("products").insertRow(1);
	var newProductCell=newProduct.insertCell(0);
	//i took the previously saved images, because not having a backend
	newProductCell.innerHTML=`
		<div>
			<img src='images/`+file.name+`'/>
			<p>`+desc+`</p>
		</div>
		<button onclick='removeItem(this)'>Remove</button>
		<button onclick='editItem(this)'>Edit</button>`;
	document.getElementById("art").value="";
	document.getElementById("description").value="";
	totalProducts+=1;
	document.getElementById("counter").innerHTML=totalProducts;
}

function removeItem(element){
	var index=element.parentNode.parentNode.rowIndex;
	document.getElementById("products").deleteRow(index);
	totalProducts-=1;
	document.getElementById("counter").innerHTML=totalProducts;
}

function editItem(element){
	modifying = !modifying;
	if (modifying){
		document.getElementById('itemSaved').style.display="block"; 
		document.getElementById('itemCancel').style.display="block";
		document.getElementById('itemAdder').style.display="none";
		var cell=element.parentNode;
		itemToModify.img=cell.getElementsByTagName('img').src;
	} else {
		document.getElementById('itemSaved').style.display="none"; 
		document.getElementById('itemCancel').style.display="none";
		document.getElementById('itemAdder').style.display="block";
		console.log("none",modifying);
	}
}

function saveItem(){

}

function discardItem(){

}
